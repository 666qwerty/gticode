#include <QCoreApplication>
#include <stdio.h>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QSet>
#include <locale>
#include <QTextCodec>
#include <QVector>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>

void ConstructItemsToParent(QString fileName, QMap<qint64, qint64>& idToParent,
                                                QMap<qint64, qint64>& idToRoot,
                                                QMap<qint64, QString>& idToName)
{
    //id extid parid name path
    QFile inputFile(fileName);
    //FILE *f=fopen("mwain.cpp","w");
    //FILE *ff=fopen("../../../../trouvus_aws/upload.sh","r");
    //FILE *ff=fopen(fileName,"r");
    //QFile inputFile("mwain.cpp");
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       in.setCodec("UTF-8");

       while (!in.atEnd())
       {
          QString line = in.readLine();
          QStringList parts = line.split(',', QString::KeepEmptyParts);
          idToParent.insert(parts[0].toLong(), parts[2].toLong());
          idToName.insert(parts[0].toLong(), parts[3]);
       }
       inputFile.close();
    }

    for(QMap<qint64, qint64>::iterator it = idToParent.begin(); it != idToParent.end(); it++)
    {
        qint64 id = it.key();
        qint64 parentId = it.value();
        while(parentId != 0)
        {
            id = parentId;
            parentId = idToParent[id];
        }
        if(id==2)
            printf("errorxx\n");
        idToRoot.insert(it.key(), id);
    }
}


void TestItemsToRoot(QMap<qint64, qint64>& idToParent,
                     QMap<qint64, qint64>& idToRoot,
                     QMap<qint64, QString>& idToName)
{
    QFile file("test.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);   // we will serialize the data into the file
    out.setCodec("UTF-8");

    int cnt = 0;
    for(QMap<qint64, qint64>::iterator it = idToParent.begin(); it != idToParent.end(); it++)
    {
        qint64 id = it.key();
        qint64 root = idToRoot[id];
        QString idName = idToName[id];
        QString rootName = idToName[root];

        QString res = QString("%1=%2 ||| %3=%4\n").arg(id).arg(idName).arg(root).arg(rootName);
        printf("%s", res.toStdString().c_str());
        out << res;
        cnt++;
        if (cnt == 10)
            break;
    }

    file.close();
}


//user_id, item_id, value, ttype, date

void ReadEvents(QString eventsName, QMap<qint64, qint64>& idToRoot,
                QMap<qint64, QMap<qint64, QVector<QPair<int, QString> > > >& bookmarks,
                QMap<qint64, QMap<qint64, QVector<QPair<double, QString> > > >& ratings,
                QMap<qint64, QMap<qint64, QVector<QPair<int, QString> > > >& views,
                QMap<qint64, int>& itemCount,
                QString& maxDt)
{
    int cnt_view_0 = 0
            ;
    int cnt_lines = 0;

    QFile inputFile(eventsName);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       in.setCodec("UTF-8");

       while (!in.atEnd())
       {
          QString line = in.readLine();
          if (line == "")
              continue;
          QStringList parts = line.split(',', QString::KeepEmptyParts);

          if (parts[3][0] == 'v' && parts[2] == "0")
          {
              cnt_view_0++;
              continue;
          }

          qint64 user = parts[0].toLong();
          qint64 topItem = idToRoot[parts[1].toLong()];
          if (topItem == 0)
              printf("error\n");

          double value =  parts[2].toDouble();
          QString dt = parts[4];

          if (maxDt == "")
              maxDt = parts[4];
          else
          {
              if (maxDt < parts[4])
                  maxDt = parts[4];
          }

          if (parts[3][0] == 'b')
          {
              bookmarks[user][topItem].push_back(qMakePair(value, dt));
          }
          if (parts[3][0] == 'v')
          {
              views[user][topItem].push_back(qMakePair(value, dt));
          }
          if (parts[3][0] == 'r')
          {
              ratings[user][topItem].push_back(qMakePair(value, dt));
          }
          itemCount[topItem]++;

          cnt_lines++;
          if (cnt_lines % 1000000 == 0)
              printf("cnt_lines == %d\n", cnt_lines);

          //if (cnt_lines >= 500000)
          //    break;
       }
       inputFile.close();
    }
    printf("cnt_view_0 == %d\n", cnt_view_0);
    //printf("maxDt == %s\n", maxDt.toString("yyyy-MM-dd HH:mm:ss+00").toStdString().c_str());
}


template<typename T>
void WriteToFile(QString path,
                 QMap<qint64, int> itemCount,
                 QMap<qint64, QMap<qint64, QVector<QPair<T, QString> > > >& data,
                 bool isMean,
                 QString startDate)
{
    QFile file(path);
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);
    out.setCodec("UTF-8");

    for (typename QMap<qint64, QMap<qint64, QVector<QPair<T, QString> > > >::iterator it = data.begin(); it != data.end(); it++)
    {
        qint64 user = it.key();
        for (typename QMap<qint64, QVector<QPair<T, QString> > >::iterator it2 = it.value().begin(); it2 != it.value().end(); it2++)
        {
            qint64 item = it2.key();
            if (itemCount[item] <= 40)
                continue;

            T sum = 0;
            int cnt = 0;
            for (int i =0; i < it2.value().size(); i++)
                if (startDate <= it2.value()[i].second)
                {
                    sum += it2.value()[i].first;
                    cnt++;
                }

            if (isMean == true)
                sum = sum / cnt;

            if (sum == 0)
                continue;

            out << user << "," << item << "," << sum << "\n";
        }
    }

    file.close();
}

void MakeDirIfNotExists(QString path)
{
    QDir dir(path);
    if (!dir.exists())
    {
        dir.mkpath(".");
    }
}

void WriteEventsToFiles(QString path,
                        QMap<qint64, QMap<qint64, QVector<QPair<int, QString> > > >& bookmarks,
                        QMap<qint64, QMap<qint64, QVector<QPair<double, QString> > > >& ratings,
                        QMap<qint64, QMap<qint64, QVector<QPair<int, QString> > > >& views,
                        QMap<qint64, int>& itemCount)
{
    QString rbv_all = path + "/rbv_all/";
    MakeDirIfNotExists(rbv_all);
    QString rbv_last3m = path + "/rbv_last3m/";
    MakeDirIfNotExists(rbv_last3m);

    QString bookmarksName_all = rbv_all + "bookmark.csv";
    QString ratingsName_all = rbv_all + "rating.csv";
    QString viewsName_all = rbv_all + "view.csv";

    QString startDate_all = "2002-01-01 01:01:01+00";
    WriteToFile<int>(bookmarksName_all, itemCount, bookmarks, false, startDate_all);
    WriteToFile<double>(ratingsName_all, itemCount, ratings, true, startDate_all);
    WriteToFile<int>(viewsName_all, itemCount, views, false, startDate_all);


    QString bookmarksName_last3m = rbv_last3m + "bookmark.csv";
    QString ratingsName_last3m = rbv_last3m + "rating.csv";
    QString viewsName_last3m = rbv_last3m + "view.csv";

    QString startDate_last3m = QDateTime::currentDateTime().addMonths(-3).toString("yyyy-MM-dd HH:mm:ss+00");
    WriteToFile<int>(bookmarksName_last3m, itemCount, bookmarks, false, startDate_last3m);
    WriteToFile<double>(ratingsName_last3m, itemCount, ratings, true, startDate_last3m);
    WriteToFile<int>(viewsName_last3m, itemCount, views, false, startDate_last3m);
}

void RefreshMaxDt(QString& maxDt, QString& eventDt)
{
    if (maxDt == "")
        maxDt = eventDt;
    else
    {
        if (maxDt < eventDt)
            maxDt = eventDt;
    }
}

void GetlastDate(QString eventsName)
{
    int cnt_lines = 0;
    //QDateTime maxDt;
    QString maxDt = "";
    QString maxDt_bookmarks = "";
    QString maxDt_ratings = "";
    QString maxDt_views = "";


    QFile inputFile(eventsName);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       in.setCodec("UTF-8");

       while (!in.atEnd())
       {
          QString line = in.readLine();
          QStringList parts = line.split(',', QString::KeepEmptyParts);

          //QDateTime dt = QDateTime::fromString(parts[4], "yyyy-MM-dd HH:mm:ss+00");
          //QString dt = parts[4], "yyyy-MM-dd HH:mm:ss+00");

          RefreshMaxDt(maxDt, parts[4]);

          if (parts[3][0] == 'b')
          {
              RefreshMaxDt(maxDt_bookmarks, parts[4]);
          }
          else if (parts[3][0] == 'r')
          {
              RefreshMaxDt(maxDt_ratings, parts[4]);
          }
          else if (parts[3][0] == 'v')
          {
              RefreshMaxDt(maxDt_views, parts[4]);
          }

          cnt_lines++;
          if (cnt_lines % 100000 == 0)
              printf("cnt_lines == %d\n", cnt_lines);
       }
       inputFile.close();
    }
    printf("maxDt == %s\n", maxDt.toStdString().c_str());
    printf("maxDt_bookmarks == %s\n", maxDt_bookmarks.toStdString().c_str());
    printf("maxDt_ratings == %s\n", maxDt_ratings.toStdString().c_str());
    printf("maxDt_views == %s\n", maxDt_views.toStdString().c_str());

}



struct item
{
    int user;
    int film;
    int rating;
    double time;
    int year;
};
int sign(double a)
{
    return ((a>0)-(a<0));
}

int compare2(const void *a,const void *b)
{
    return sign((((item *)a)->time-((item *)b)->time));
}
struct sd
{
    char s[100];
    int num;
};

void ConstructLstmEvents(QString eventsPath, QMap<qint64, qint64>& idToRoot, QString eventsLstmPath)
{
    QFile fileLSTM(eventsLstmPath);
    fileLSTM.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fileLSTM);
    out.setCodec("UTF-8");
    int n=152727499;//92;//
    item *mas=new(item[n]);
    int i=0;
    int cnt_lines = 0;
    int cnt_view_0 = 0;
    sd massd[200];
    int months[200];
    int vmonths[200];
    int bmonths[200];
    int rmonths[200];
    for (int a=0;a<200;a++)
    {
        months[a]=0;
        vmonths[a]=0;
        bmonths[a]=0;
        rmonths[a]=0;
        massd[a].s[0]=0;
    }
    int nsd=0;
    FILE *log=fopen("log","w");
    QFile inputFile(eventsPath);
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       in.setCodec("UTF-8");

       while (!in.atEnd())
       {
          QString line = in.readLine();
          if (line == "")
              continue;
          QStringList parts = line.split(',', QString::KeepEmptyParts);

          if (parts[3][0] == 'v' && parts[2] == "0")
          {
              cnt_view_0++;
              continue;
          }

          //qint64 user = parts[0].toLong();
          qint64 topItem = idToRoot[parts[1].toLong()];
          if(topItem==2)
              printf("errorx\n");
          if (topItem == 0)
              printf("error\n");
          sscanf((parts[0].toUtf8()).constData(),"%d",&mas[i].user);
          sscanf(parts[2].toUtf8().constData(),"%d",&mas[i].rating);
          mas[i].film=topItem;
          int d1,d2,d3,d4,d5;
          sscanf(parts[4].toUtf8().constData(),"%d-%d-%d %d:%d",&d1,&d2,&d3,&d4,&d5);

          months[(d1-2000)*12+d2]++;
          if(i==10000)
              printf("icanprint\n");
          if(!strcmp(parts[3].toUtf8().constData(),"view"))
              mas[i].rating=-1;
              vmonths[(d1-2000)*12+d2]++;
          if(!strcmp(parts[3].toUtf8().constData(),"bookmark"))
              mas[i].rating=-2;
              bmonths[(d1-2000)*12+d2]++;
          if(!strcmp(parts[3].toUtf8().constData(),"rating"))
              rmonths[(d1-2000)*12+d2]++;
          bool flag=false;
          char ss[100];
          sprintf(ss,"%s%s",parts[3].toUtf8().constData(),parts[2].toUtf8().constData());
          for(int a=0;a<nsd;a++)
              if(!strcmp(massd[a].s,ss))
              {
                  flag=true;
                  massd[a].num++;
              }
          if(!flag)
          {
              strcpy(massd[nsd].s,ss);
              massd[nsd].num=1;
              nsd++;
          }
          mas[i].time=((((((double)mas[i].user)*2200+d1)*370+d2)*30+d3)*24+d4)*60+d5;
          mas[i].year=d1;
          QString res = QString("%1,%2,%3,%4,%5\n").arg(parts[0]).arg(topItem).arg(parts[2]).arg(parts[3]).arg(parts[4]);
          out << res;
          i++;
          //if(d1<2014)
            //i--;
          cnt_lines++;
          if (cnt_lines % 10000000 == 0)
          {
              fprintf(log,"cnt_lines == %d\n", cnt_lines);
              for(int a=0;a<200;a++)
                  fprintf(log,"all_year%dmonth%d_%d\n",a/12,a%12,months[a]);
              for(int a=0;a<200;a++)
                  fprintf(log,"view_year%dmonth%d_%d\n",a/12,a%12,vmonths[a]);
              for(int a=0;a<200;a++)
                  fprintf(log,"bookmark_year%dmonth%d_%d\n",a/12,a%12,bmonths[a]);
              for(int a=0;a<200;a++)
                  fprintf(log,"rating_year%dmonth%d_%d\n",a/12,a%12,rmonths[a]);
              for(int a=0;a<nsd;a++)
                  fprintf(log,"%s_%d\n", massd[a].s, massd[a].num);
          }
          //if (cnt_lines >= 500000)
          //    break;
       }
       inputFile.close();
    }
    fileLSTM.close();
    n=i+1;
    qsort(mas,n,sizeof(item),compare2);
    QFile fileLSTM2("eventsLstmPath2q");
    fileLSTM2.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out2(&fileLSTM2);
    out2.setCodec("UTF-8");
    for(int a=0;a<n;a++)
    {
        if(mas[a].film==2)
            printf("errorxxx\n");
        QString res = QString("%1\t%2\t%3\n").arg(mas[a].user).arg(mas[a].film).arg(mas[a].rating);
        out2 << res;
    }
    QFile fileLSTM3("eventsLstmPathvvq");
    fileLSTM3.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out3(&fileLSTM3);
    out3.setCodec("UTF-8");
    for(int a=0;a<n;a++)
    {
            QString res = QString("%1\t%2\t%3\t%4\n").arg(mas[a].user).arg(mas[a].film).arg(mas[a].rating).arg(mas[a].year);
            out3 << res;
    }
    QFile fileLSTM4("eventsLstmPathbq");
    fileLSTM4.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out4(&fileLSTM4);
    out4.setCodec("UTF-8");
    for(int a=0;a<n;a++)
    {
        if(mas[a].rating==-2)
        {
            QString res = QString("%1\t%2\t%3\n").arg(mas[a].user).arg(mas[a].film).arg(mas[a].rating);
            out4 << res;
        }
    }
    QFile fileLSTM5("eventsLstmPathrq");
    fileLSTM5.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out5(&fileLSTM5);
    out5.setCodec("UTF-8");
    for(int a=0;a<n;a++)
    {
        if(mas[a].rating>0)
        {
            QString res = QString("%1\t%2\t%3\n").arg(mas[a].user).arg(mas[a].film).arg(mas[a].rating);
            out5 << res;
        }
    }
    /*int nn=-1;
    for(int a=0;a<n;a++)
    {
        if(nn!=mas[a].user/1000)
        {
            nn=mas[a].user/1000;
            fileLSTM2.close();
            char s[100];
            sprintf(s,"%duseritems",nn);
            fileLSTM2(s);
            fileLSTM2.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream out2(&fileLSTM2);
            out2.setCodec("UTF-8");
        }
        QString res = QString("%1\t%2\t%3\n").arg(mas[a].user).arg(mas[a].film).arg(mas[a].rating).arg(mas[a].time);
        out2 << res;
    }*/
    fileLSTM2.close();
    delete[] mas;
    fprintf(log,"cnt_view_0 == %d\n", cnt_view_0);
    fclose(log);
}


struct film
{
    qint64 num;
    QString name;
    film();
};
film::film()
{
}
int compare(const void *a,const void *b)
{
    return(((film *)a)->num-((film *)b)->num);
}

void ConstructLstmItems(QMap<qint64, qint64>& idToRoot, QMap<qint64, QString>& idToName, QString itemsLstmPath)
{
    QSet<qint64> uniqRootIds;
   // int n=uniqRootIds.size();
    int i=0;
    for (QMap<qint64, qint64>::iterator it = idToRoot.begin(); it != idToRoot.end(); it++)
    {

        uniqRootIds.insert(it.value());
    }

    QFile fileLSTM(itemsLstmPath);
    fileLSTM.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&fileLSTM);
    out.setCodec("UTF-8");
    int n=uniqRootIds.size();
    film *mas=new(film[n]);

    for (QSet<qint64>::iterator it = uniqRootIds.begin(); it != uniqRootIds.end(); it++)
    {
        mas[i].num=*it;
        mas[i].name=idToName[mas[i].num];
        i++;
        QString res = QString("%1,%2\n").arg(*it).arg(idToName[*it]);
        out << res;
    }
    fileLSTM.close();
    qsort(mas,n,sizeof(film),compare);
    QFile fileLSTM2("itemsLstmPath");
    fileLSTM2.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out2(&fileLSTM2);
    out2.setCodec("UTF-8");
    //int nn=-1;
    for(int a=0;a<n;a++)
    {
//        if(mas[a].num!=nn)
 //       {
        QString res = QString("%1|%2\n").arg(mas[a].num).arg(mas[a].name);
        out2 << res;
 //       nn=mas[a].num;
//        }
    }
    fileLSTM2.close();
    delete[] mas;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    /*char s[100];
    FILE *f=fopen("../../../../trouvus_aws/items_lstm.csv","r");
    int i=0;
    film fms[83000];
    while(fgets(fms[i].mas,101,f))
    {
        char *n=strchr(fms[i].mas,',');
        n[0]=0;
        fms[i].name=n+1;
        sscanf(fms[i].mas,"%d",&fms[i].num);
        i++;
        printf("%d\n",i);
    }
    i=i;*/
    /*QString pathStart = "/src/data/trouvus-dump-start/";
    QString eventsStartName = "/src/data/trouvus-dump-start/trouvus-06-11-2015-events.csv";
    QString path = "/src/data/trouvus-dump-2015-11-11/";
    QString itemsName = "/src/data/trouvus-dump-2015-11-11/items.csv";
    QString eventsName = "/src/data/trouvus-dump-2015-11-11/events.csv";*/

    QCoreApplication::setApplicationName("trouvus-prepare-data");
    QCoreApplication::setApplicationVersion("1.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("trouvus-prepare-data");
    parser.addHelpOption();
    parser.addVersionOption();

    QCommandLineOption omode("mode", "mode", "work mode (lastdt/prepare/lstm)", "lstm");
    QCommandLineOption oeventsStartName("eventsStartName", "full path to huge events file", "full path to huge events file", "../../../../trouvus_aws/trouvus-11-11-2015-events.csv");
    QCommandLineOption oitemsName("itemsName", "full path to items file", "full path to items file", "../../../../trouvus_aws/trouvus-11-11-2015-items.csv");
    QCommandLineOption oeventsLstmName("eventsLstmName", "full path res events_lstm file", "full path res events_lstm file", "../../../../trouvus_aws/events_lstm.csv");
    QCommandLineOption oitemsLstmName("itemsLstmName", "full path res items_lstm file", "full path res items_lstm file", "../../../../trouvus_aws/items_lstm.csv");

    QCommandLineOption orestDir("restDir", "full path to rest dir", "full path to rest dir", "../../../../trouvus_aws/");
    QCommandLineOption oeventsName("eventsName", "full path to rest events file", " full path to rest events file", "");
    //QCommandLineOption oitemsName("itemsName", "full path to items file", "full path to items file", "../../../../trouvus_aws/trouvus-11-11-2015-items.csv");
/*    QCommandLineOption omode("mode", "mode", "work mode (lastdt/prepare/lstm)", "lstm");
    QCommandLineOption oeventsStartName("eventsStartName", "full path to huge events file", "full path to huge events file", "../../../../trouvus_aws/tre.csv");
    QCommandLineOption oitemsName("itemsName", "full path to items file", "full path to items file", "../../../../trouvus_aws/tri.csv");
    QCommandLineOption oeventsLstmName("eventsLstmName", "full path res events_lstm file", "full path res events_lstm file", "../../../../trouvus_aws/ev_lstm.csv");
    QCommandLineOption oitemsLstmName("itemsLstmName", "full path res items_lstm file", "full path res items_lstm file", "../../../../trouvus_aws/it_lstm.csv");

    QCommandLineOption orestDir("restDir", "full path to rest dir", "full path to rest dir", "../../../../trouvus_aws/");
    QCommandLineOption oeventsName("eventsName", "full path to rest events file", " full path to rest events file", "");
    //QCommandLineOption oitemsName("itemsName", "full path to items file", "full path to items file", "../../../../trouvus_aws/tri.csv");
*/
    parser.addOption(omode);
    parser.addOption(oeventsStartName);
    parser.addOption(oeventsLstmName);
    parser.addOption(oitemsLstmName);
    parser.addOption(orestDir);
    parser.addOption(oeventsName);
    parser.addOption(oitemsName);

    parser.process(a);

    QString eventsStartName;
    QString path;
    QString itemsName;
    QString eventsName;

    QString itemsLstmName;
    QString eventsLstmName;


    QMap<qint64, qint64> idToParent;
    QMap<qint64, qint64> idToRoot;
    QMap<qint64, QString> idToName;

    QString mode = parser.value(omode);
    printf("mode:%s\n", mode.toStdString().c_str());

    eventsStartName = parser.value(oeventsStartName);

    if (mode == "lastdt")
    {
        GetlastDate(eventsStartName);
        return 0;
    }

    if (mode == "lstm")
    {
        itemsName = parser.value(oitemsName);
        printf("ConstructItemsToParent\n");
        ConstructItemsToParent(itemsName, idToParent, idToRoot, idToName);

        eventsLstmName = parser.value(oeventsLstmName);
        printf("ConstructLstmEvents");
        ConstructLstmEvents(eventsStartName, idToRoot, eventsLstmName);

        itemsLstmName = parser.value(oitemsLstmName);
        printf("ConstructLstmItems");
        ConstructLstmItems(idToRoot, idToName, itemsLstmName);

        printf("lstm all done\n");
        return 0;
    }

    path = parser.value(orestDir);
    itemsName = parser.value(oitemsName);
    eventsName = parser.value(oeventsName);





    printf("ConstructItemsToParent\n");
    ConstructItemsToParent(itemsName, idToParent, idToRoot, idToName);


    QMap<qint64, QMap<qint64, QVector<QPair<int, QString> > > > bookmarks;
    QMap<qint64, QMap<qint64, QVector<QPair<double, QString> > > > ratings;
    QMap<qint64, QMap<qint64, QVector<QPair<int, QString> > > > views;
    QMap<qint64, int> itemCount;
    QString maxStartDt = "";
    printf("ReadEvents-start\n");
    ReadEvents(eventsStartName, idToRoot, bookmarks, ratings, views, itemCount, maxStartDt);

    if (eventsName.isEmpty() == false)
    {
        printf("ReadEvents-last\n");
        QString maxDt = "";
        ReadEvents(eventsName, idToRoot, bookmarks, ratings, views, itemCount, maxDt);
    }


    printf("WriteEventsToFiles\n");
    WriteEventsToFiles(path, bookmarks, ratings, views, itemCount);

    printf("finished\n");
    return 0;
}
