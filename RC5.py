# coding: utf-8

import numpy as np
import pandas as pd
import pickle
import time
import xgboost as xgb

from scipy.sparse import hstack
from datetime import datetime
from sklearn.metrics import roc_auc_score, log_loss

from dataset import *
from process_columns import *

class LedEstimador():

    # known_columns = ['Phrase', 'query', 'bannerText', 'weekDay', 'dayPart', 'CampaignID', 'SiteID', 'mode', 'type', 'PhraseCount', 'queryCount', 'position']
    #known_columns = ['Phrase', 'weekDay', 'dayPart', 'CampaignID', 'PositionType', 'position', 'PhraseCount', 'queryCount']
    known_columns = ['Phrase', 'CampaignID','PhraseCount', 'queryCount', 'position', 'month', 'dayPart2', 'weekDay', 'dt', 'siteID']

    def prepare_data(self, data):
        if 'criterion' in data.columns:
            data = extract_id(data, 'criterion', 'Phrase')
        cleaned = clean(data, drop_empty_query=False, clean_phrase=True)

        if 'Phrase' in cleaned.columns:
            cleaned = extract_word_count(cleaned, 'Phrase', 'PhraseCount')

        if 'query' in cleaned.columns:
            cleaned = extract_word_count(cleaned, 'query', 'queryCount')

        if 'Date' in cleaned.columns and 'dayPart' not in cleaned.columns:
            cleaned = extract_date_daypart(cleaned, 'Date', 'dayPart')

        if 'Date' in cleaned.columns and 'weekDay' not in cleaned.columns:
            cleaned = extract_date_week(cleaned, 'Date', 'weekDay')

        if 'Date' in cleaned.columns:
            cleaned = extract_date_daypart2(cleaned, 'Date', 'dayPart2')

        if 'Date' in cleaned.columns:
            cleaned = extract_date_m(cleaned, 'Date', 'month')
        if 'bannerLink' in cleaned.columns:
            cleaned = extract_site(cleaned, 'bannerLink', 'bL')

        if 'SourceType' in cleaned.columns:
            cleaned = extract_dt(cleaned, 'SourceType', 'st')

        if 'DeviceType' in cleaned.columns:
            cleaned = extract_dt(cleaned, 'DeviceType', 'dt')
        if 'bannerType' in cleaned.columns:
            cleaned = extract_dt(cleaned, 'bannerType', 'bt')
        if 'match_type' in cleaned.columns:
            cleaned = extract_dt(cleaned, 'match_type', 'mt')

        return cleaned

    def preprocess_fit(self, cleaned):
        train_columns = []
        for column in self.known_columns:
            if not column in cleaned.columns:
                continue
            train, estimator = process_column_fit(column, cleaned[[column]])
            train_columns.append(train)

            if estimator is not None:
                with open(self.path+'estimator-'+column+'.pk', 'wb') as fin:
                    pickle.dump(estimator, fin)

        X = hstack(train_columns)
        return X

    def preprocess_predict(self, cleaned):
        train_columns = []
        for column in self.known_columns:
            if not column in cleaned.columns:
                continue

            try:
                with open(self.path+'estimator-'+column+'.pk', 'rb') as fin:
                    estimator = pickle.load(fin)
            except:
                estimator = None

            train = process_column_predict(column, cleaned[[column]], estimator)
            train_columns.append(train)

        X = hstack(train_columns)
        return X

    def fit(self, file, path):
        self.path = path

        raw = load_file(file)
        cleaned = self.prepare_data(raw)

        X = self.preprocess_fit(cleaned)
        Y = cleaned['callID'].values

        data = xgb.DMatrix(X, label = Y)
        params = { 'silent':True, 'objective':'binary:logistic', 'booster':'gblinear', 'alpha': 0.001, 'lambda': 1 }
        bst = xgb.train(params, data, 10)
        bst.save_model(path+"model.xgboost")

    def predict(self, file, path):
        self.path = path

        raw = load_file(file)
        orig = raw.copy(deep=True)
        cleaned = self.prepare_data(raw)

        X = self.preprocess_predict(cleaned)
        data = xgb.DMatrix(X)
        bst = xgb.Booster()
        bst.load_model(path+"model.xgboost")
        preds = bst.predict(data)

        orig['pred'] = preds

        if 'callID' in cleaned.columns:
            Y = cleaned['callID'].values
            print roc_auc_score(Y, preds)
            print log_loss(Y, preds)
        else:
            print orig.to_csv(index_label=False, index=False)
            # for i in range(len(preds)):
            #     print(preds[i])

        return preds