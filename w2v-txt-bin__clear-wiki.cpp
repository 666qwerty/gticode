#include <QCoreApplication>
#include <stdio.h>
//#include <random>
#include <stdio.h>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QSet>
#include <locale>
#include <QTextCodec>
#include <QVector>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <math.h>

int i1=0;
int i2=0;
int i3=0;
int i4=0;

void reid(char *fil)
{
    QFile mlog("mlog");
    QFile ilp("itemsLstmPath2");
    QMap<qint64, QString> idToText;
    if (ilp.open(QIODevice::ReadOnly))
    {
       QTextStream in(&ilp);
       in.setCodec("UTF-8");
       while (!in.atEnd())
       {

          QString line = in.readLine();
          QStringList parts = line.split('|', QString::KeepEmptyParts);
          idToText.insert(parts[0].toLong(), parts[1]);
       }
       ilp.close();
    }
    char s[1000];
    sprintf(s,"newitems_%s",fil);
    QFile fileLSTM2(s);
    fileLSTM2.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out2(&fileLSTM2);
    out2.setCodec("UTF-8");
    if (mlog.open(QIODevice::ReadOnly))
    {
        QTextStream in(&mlog);
        in.setCodec("UTF-8");
        while (!in.atEnd())
        {
           QString line = in.readLine();
           QStringList parts = line.split(' ', QString::KeepEmptyParts);
           QString res = QString("%1|%2|%3\n").arg(parts[0]).arg(parts[1]).arg(idToText[parts[0].toLong()]);
           out2 << res;
        }
        mlog.close();
    }
    fileLSTM2.close();
}


class filminfo
{
public:
    int filmfreq,filmfreqv,filmfreqb,filmfreqr;
    double filmfreqavr;
};
class userinfo
{
public:
    int userlen;
    bool train;
    int id;
    //int filmfreq,filmfreqv,filmfreqb,filmfreqr
};
class Item
{
    int id;
};
class User;
class Watch
{
public:
    int iitem;
    double rating;
};
class User
{
public:
    int id;
    int nw;
    Watch watchs[100000];
    User();
    User *rev(double left,double right);
    User *idf(double left,double right);
    User *perm(double left,double right);
    User *ff(double left,double right,filminfo *ifilm,int nfreq);
    User *aug_freq(double left,double right,filminfo *ifilm,int nfreq,double wl,double l1);
    void uir(FILE *f);
};
User::User()
{
    nw=0;
}
void User::uir(FILE *f)
{
    for(int i=0;i<nw;i++)
    {
        if(1097==watchs[i].iitem)
            i4++;
        fprintf(f,"%d\t%d\t%lf\n",id,watchs[i].iitem,watchs[i].rating);
    }
}
User *User::rev(double left,double right)
{
    User *nu=new(User);
    nu->id=id;
    int q=nw-1;

    for(int i=0;i<nw;i++)
    {
        if(watchs[i].rating<left)
           nu->watchs[i]=watchs[i];
        else
        if(watchs[i].rating>right)
            nu->watchs[i]=watchs[i];
        else
        {
            while((watchs[q].rating<left)||(watchs[q].rating>right))
                q--;
            nu->watchs[i]=watchs[q];
            q--;
        }
    }
    nu->nw=nw;
    return nu;
}
User *User::idf(double left,double right)
{
    User *nu=new(User);
    nu->id=id;
    int q=0;
    for(int i=0;i<nw;i++)
    {
        if(watchs[i].rating<left);
        else
        if(watchs[i].rating>right);
        else
        {
            nu->watchs[q]=watchs[i];
            q++;
        }
    }
    nu->nw=q;
    return nu;
}
User *User::ff(double left,double right,filminfo *ifilm,int nfreq)
{
    User *nu=new(User);
    nu->id=id;
    int q=0;
    for(int i=0;i<nw;i++)
    {
        if(nw==4154)
            if(i==4151)
            {
                i2=i;
            }
        if(1097==watchs[i].iitem)
            i2++;
        if(watchs[i].rating<left);
        else
            if(watchs[i].rating>right);
            else
                if(ifilm[watchs[i].iitem].filmfreqr<nfreq);
                else
                    //if(ifilm[watchs[i].iitem].filmfreqv>100000);
                    //else
        {
            nu->watchs[q]=watchs[i];
            q++;
        }
    }
    nu->nw=q;
    return nu;
}

bool need_remove(double freq,double nfreq,double wl,double l1)
{
    double rr =  double(rand())/(1<<30)/2;
    if(freq<(nfreq-wl))
        return 1;
    if (rr > (1-(1-(1.0*nfreq/freq))*l1))
        return 1;
    return 0;
}
bool need_remove2(double freq,double nfreq,double wl,double l1)
{
    double rr =  double(rand())/(1<<30)/2;
    if(freq<(nfreq-wl))
        return 1;
    if (rr > (1-(1-(log(freq)/log(nfreq)*nfreq/freq))*l1))
        return 1;
    return 0;
}
User *User::aug_freq(double left,double right,filminfo *ifilm,int nfreq,double wl,double l1)
{
    User *nu=new(User);
    nu->id=id;
    int q=0;
    for(int i=0;i<nw;i++)
    {
        if(watchs[i].rating<left);
        else
            if(watchs[i].rating>right);
            else
        if(!need_remove2(ifilm[watchs[i].iitem].filmfreqr,nfreq,wl,l1))
        {
            nu->watchs[q]=watchs[i];
            q++;
        }
    }
    nu->nw=q;
    return nu;
}
User *User::perm(double left,double right)
{
    User *nu=new(User);
    nu->id=id;
    int q=0;


    int *mas=new(int[nw]);
    for(int i=0;i<nw;i++)
    {
        if(watchs[i].rating<left);
        else
        if(watchs[i].rating>right);
        else
        {
            mas[q]=i;
            q++;
        }
    }
    for(int i=1;i<q;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    q=0;
    for(int i=0;i<nw;i++)
    {
        if(watchs[i].rating<left)
           nu->watchs[i]=watchs[i];
        else
        if(watchs[i].rating>right)
            nu->watchs[i]=watchs[i];
        else
        {
            nu->watchs[i]=watchs[mas[q]];
            q++;
        }
    }
    nu->nw=nw;
    return nu;
}
class TG
{
public:
    int nusers,nfilms;
    //User *users;
    char filename[100];
    filminfo *ifilm;
    userinfo *iuser;
    int nuserlen,nfilmfreq;
    void parse(char *file);
    void count(char *file,int nus,int nuf);
    TG *rev(double left,double right);
    TG *perm(double left,double right);
    TG *idf(double left,double right);
    TG *userfreq(double left,double right,double nfreq,double wl,double l1);
    TG *filmfreq_aug(double left,double right,double nfreq,double wl,double l1);
    TG *filmfreq(double left,double right,double nfreq,double wl,double l1);
    TG *copy();
    TG *nyam(TG *tg);
    void generate(double left,double right);
    void generate_a(double left,double right,double gate);
    void fastgenerate(double left,double right);
    void abgenerate(double left,double right);
    void fastgeneratepart(int part,double left,double right);
    void fastgeneratex(double left,double right);
    void fastgenerate_a(double left,double right,double gate);
};
char *idtostr;
void TG::count(char *file,int nus=1000000,int nuf=1000000)
{
    strcpy(filename,file);
    FILE *f=fopen(file,"r");
    char s[100];
    int cu=-1;
    int trueu=-1;
    nuserlen=nus;
    nfilmfreq=nuf;
    iuser=new(userinfo[nus]);
    ifilm=new(filminfo[nuf]);
    for(int i=0;i<nus;i++)
    {
        iuser[i].userlen=0;
    }
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
        }
        iuser[trueu].userlen++;
        iuser[trueu].id=u;
        iuser[trueu].train=(rand()%9);
        ifilm[i].filmfreq++;
        if(r==-2)
            ifilm[i].filmfreqb++;
        else
        if(r==-1)
            ifilm[i].filmfreqv++;
        else
        {
            ifilm[i].filmfreqavr+=r;
            ifilm[i].filmfreqr++;
        }
    }
    fclose(f);
    nusers=trueu+1;
    //users=new(User[trueu]);

    /*f=fopen("config","w");
    fprintf(f,"%d\n",trueu);
    for(int i=0;i<trueu;i++)
    {
        fprintf(f,"%d\n",userlen[i]);
    }
    fprintf(f,"%d\n",1000000);
    for(int i=0;i<1000000;i++)
    {
        if(ifilm[i].filmfreqr)
            fprintf(f,"%d\t%d\t%d\t%d\t%lf\n",ifilm[i].filmfreq,ifilm[i].filmfreqv,ifilm[i].filmfreqb,ifilm[i].filmfreqr,ifilm[i].filmfreqavr/ifilm[i].filmfreqr);
        else
            fprintf(f,"%d\t%d\t%d\t%d\t%lf\n",ifilm[i].filmfreq,ifilm[i].filmfreqv,ifilm[i].filmfreqb,ifilm[i].filmfreqr,0.0);
    }*/
    FILE *flog=fopen("mlog","w");
    int qqq=0;
    for(int i=0;i<nuf;i++)
    {
        if(ifilm[i].filmfreqr)
            ifilm[i].filmfreqavr=ifilm[i].filmfreqavr/ifilm[i].filmfreqr;
        nfilms=0;
        if(ifilm[i].filmfreq)
        {
            qqq+=ifilm[i].filmfreq;
            fprintf(flog,"%d %d\n",i,ifilm[i].filmfreq);
            nfilms++;
        }
    }
    fclose(flog);

}
int grrm(int q)
{
    if(q<50)
        return (q+0x31);
    else
        return (q+0xC0-50);
}

void idtostr_init(filminfo *ifilm,int nfilmfreq)
{
    int qqq=0;
    idtostr=new(char[3*1000000]);
    int i=0;
    FILE *fo=fopen("newitems","w");
    while(i<nfilmfreq)
    {
        if(ifilm[i].filmfreq)
        {
            idtostr[3*i]=grrm(qqq%100);
            idtostr[3*i+1]=grrm(qqq/100);
            idtostr[3*i+2]=0;
            qqq++;
            fprintf(fo,"%s|%d\n",&idtostr[3*i],i);
        }
        i++;
    }
    fclose(fo);
    /*char s[1000];
    char ss[1000];
    while(fgets(s,1000,fi)!=0)
    {
        int a;
        sscanf(s,"%d|%s\n",&a,ss);
        fprintf(fo,"%s|%s\n",&idtostr[a],ss);
    }
    fclose(fi);*/
}
TG *TG::nyam(TG *tg)
{
    char s[100];
    FILE *f=fopen(tg->filename,"r");
    FILE *fo=fopen(filename,"a");
    while(fgets(s,100,f)!=0)
    {
        fprintf(fo,"%s",s);
    }
    fclose(f);
    fclose(fo);
    count(filename,nusers*2,1000000);
    return this;
}

class userdict
{
public:
    int len;
    int *mas;
    double *r;
    ~userdict();
};
userdict::~userdict()
{
    delete[] mas;
    delete[] r;
}
void TG::fastgeneratex(double left=-1000,double right=1000)
{
    int *mas=new(int[nusers]);
    userdict *ud=new(userdict[nusers]);
    for(int i=0;i<nusers;i++)
    {
        ud[i].len=0;
        ud[i].mas=new(int[iuser[i].userlen]);
        ud[i].r=new(double[iuser[i].userlen]);
    }
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    char s[1000];
    sprintf(s,"%s_train",filename);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s_valid",filename);
    FILE *fv=fopen(s,"w");
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
        }
        if((left<r)&&(right>r))
        {
            ud[trueu].mas[ud[trueu].len]=i;
            ud[trueu].r[ud[trueu].len]=r;
            ud[trueu].len++;
        }
    }
    fclose(f);
    //int *mas=new(int[nusers]);
    idtostr_init(ifilm,nfilmfreq);
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        if(iuser[mas[i]].train)
            nf=ft;
        else
            nf=fv;
        for(int a=0;a<ud[mas[i]].len;a++)
            fprintf(nf,"%s ",&(idtostr[ud[mas[i]].mas[a]*3]));
        fprintf(nf,"0 ");
    }
    fclose(ft);
    fclose(fv);
    delete[] mas;
    delete[] ud;
}
void TG::fastgenerate(double left=-1000,double right=1000)
{
    int *mas=new(int[nusers]);
    userdict *ud=new(userdict[nusers]);
    for(int i=0;i<nusers;i++)
    {
        ud[i].len=0;
        ud[i].mas=new(int[iuser[i].userlen]);
        ud[i].r=new(double[iuser[i].userlen]);
    }
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    char s[1000];
    sprintf(s,"%s_train",filename);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s_valid",filename);
    FILE *fv=fopen(s,"w");
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
        }
        if((left<r)&&(right>r))
        {
            ud[trueu].mas[ud[trueu].len]=i;
            ud[trueu].r[ud[trueu].len]=r;
            ud[trueu].len++;
        }
    }
    fclose(f);
    //int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        if(iuser[mas[i]].train)
            nf=ft;
        else
            nf=fv;
        for(int a=0;a<ud[mas[i]].len;a++)
            fprintf(nf,"%d ",ud[mas[i]].mas[a]);
        fprintf(nf,"0 ");
    }
    fclose(ft);
    fclose(fv);
    delete[] mas;
    delete[] ud;
    reid(filename);
    this->count(filename,3000000);
}
void TG::abgenerate(double left=-1000,double right=1000)
{
    int *mas=new(int[nusers]);
    userdict *ud=new(userdict[nusers]);
    for(int i=0;i<nusers;i++)
    {
        ud[i].len=0;
        ud[i].mas=new(int[iuser[i].userlen]);
        ud[i].r=new(double[iuser[i].userlen]);
    }
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    /*for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }*/
    char s[1000];
    sprintf(s,"%s_ab",filename);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
        }
        if((left<r)&&(right>r))
        {
            ud[trueu].mas[ud[trueu].len]=i;
            ud[trueu].r[ud[trueu].len]=r;
            ud[trueu].len++;
        }
    }
    fclose(f);
    //int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        nf=ft;
        fprintf(nf,"%d",iuser[mas[i]].id);
        for(int a=0;a<ud[mas[i]].len;a++)
            fprintf(nf," %d",ud[mas[i]].mas[a]);
        fprintf(nf,"\n");
    }
    fclose(ft);
    delete[] mas;
    delete[] ud;
    reid(filename);
    this->count(filename,3000000);
}
void TG::fastgeneratepart(int part,double left=-1000,double right=1000)
{
    int *mas=new(int[nusers]);
    userdict *ud=new(userdict[nusers]);
    for(int i=0;i<nusers;i++)
    {
        ud[i].len=0;
        ud[i].mas=new(int[iuser[i].userlen]);
        ud[i].r=new(double[iuser[i].userlen]);
    }
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    char s[1000];
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
        }
        if((left<r)&&(right>r))
        {
            ud[trueu].mas[ud[trueu].len]=i;
            ud[trueu].r[ud[trueu].len]=r;
            ud[trueu].len++;
        }
    }
    fclose(f);
    //int *mas=new(int[nusers]);
    int npart=0;
    sprintf(s,"%s_train_%d",filename,npart);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s_valid",filename);
    FILE *fv=fopen(s,"w");
    int len=0;
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        if(iuser[mas[i]].train)
            nf=ft;
        else
            nf=fv;
        for(int a=0;a<ud[mas[i]].len;a++)
        {
            fprintf(nf,"%d.0 ",ud[mas[i]].mas[a]);
            len++;
        }
        fprintf(nf,"0 ");
        if(len*9>part)
        {
            len=0;
            npart++;
            fclose(ft);
            sprintf(s,"%s_train_%d",filename,npart);
            ft=fopen(s,"w");
        }
    }
    fclose(ft);
    fclose(fv);
    delete[] mas;
    delete[] ud;
}
void TG::fastgenerate_a(double left,double right,double gate)
{
    int *mas=new(int[nusers]);
    userdict *ud=new(userdict[nusers]);
    for(int i=0;i<nusers;i++)
    {
        ud[i].len=0;
        ud[i].mas=new(int[iuser[i].userlen]);
        ud[i].r=new(double[iuser[i].userlen]);
    }
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    char s[1000];
    sprintf(s,"%s_traina",filename);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s_valida",filename);
    FILE *fv=fopen(s,"w");
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
        }
        if((left<r)&&(right>r))
        {
            ud[trueu].mas[ud[trueu].len]=i;
            ud[trueu].r[ud[trueu].len]=r;
            ud[trueu].len++;
        }
    }
    fclose(f);
    //int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        if(iuser[mas[i]].train)
            nf=ft;
        else
            nf=fv;
        for(int a=0;a<ud[mas[i]].len;a++)
            if(ud[mas[i]].r[a]<gate)
                fprintf(nf,"%d ",ud[mas[i]].mas[a]);
        fprintf(nf,"a ");
        for(int a=0;a<ud[mas[i]].len;a++)
            if(ud[mas[i]].r[a]>=gate)
                fprintf(nf,"%d ",ud[mas[i]].mas[a]);
        fprintf(nf,"0 ");
    }
    fclose(ft);
    fclose(fv);
    delete[] mas;
    delete[] ud;
}
void TG::generate(double left,double right)
{
    int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    char s[1000];
    sprintf(s,"%s_train",filename);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s_valid",filename);
    FILE *fv=fopen(s,"w");
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    FILE *temp=0;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            if(temp)
            {
                fclose(temp);
            }
            cu=u;
            trueu++;
            sprintf(s,"user%d",trueu);
            temp=fopen(s,"w");
        }
        if((left<r)&&(right>r))
            fprintf(temp,"%d.0 \n",i);
    }
    fclose(temp);
    fclose(f);
    //int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        if(iuser[mas[i]].train)
            nf=ft;
        else
            nf=fv;
        sprintf(s,"user%d",mas[i]);
        f=fopen(s,"r");
        while(fgets(s,100,f)!=0)
        {
            s[strlen(s)-1]=0;
            fprintf(nf,"%s",s);
        }
        fprintf(nf,"0 ");
        fclose(f);
    }
    fclose(ft);
    fclose(fv);
    for(int i=3;i<nusers;i++)
    {
        sprintf(s,"user%d",i);
        remove(s);
    }
    delete[] mas;
}
void TG::generate_a(double left,double right,double gate)
{
    int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        mas[i]=i;
    }
    for(int i=1;i<nusers;i++)
    {
        int j=random()%(i+1);
        int l=mas[i];
        mas[i]=mas[j];
        mas[j]=l;
    }
    char s[1000];
    sprintf(s,"%s_traina",filename);
    FILE *ft=fopen(s,"w");
    sprintf(s,"%s_valida",filename);
    FILE *fv=fopen(s,"w");
    sprintf(s,"%s",filename);
    FILE *f=fopen(s,"r");
    int cu=-1;
    int trueu=-1;
    FILE *temp=0;
    while(fgets(s,100,f)!=0)
    {
        int u,i;
        double r;
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            if(temp)
            {
                fclose(temp);
            }
            cu=u;
            trueu++;
            sprintf(s,"user%d",trueu);
            temp=fopen(s,"w");
        }
        if((left<r)&&(right>r))
            fprintf(temp,"%d %lf\n",i,r);
    }
    fclose(temp);
    fclose(f);
    //int *mas=new(int[nusers]);
    for(int i=0;i<nusers;i++)
    {
        FILE *nf;
        if(iuser[mas[i]].train)
            nf=ft;
        else
            nf=fv;
        int itemf;
        double rat;
        sprintf(s,"user%d",mas[i]);
        f=fopen(s,"r");
        while(fgets(s,100,f)!=0)
        {
            if(sscanf(s,"%d %lf",&itemf,&rat));
            if(rat<gate)
                fprintf(nf,"%d.0 ",itemf);
        }
        fprintf(nf,"a ");
        fclose(f);
        sprintf(s,"user%d",mas[i]);
        f=fopen(s,"r");
        while(fgets(s,100,f)!=0)
        {
            if(sscanf(s,"%d %lf",&itemf,&rat));
            if(rat>=gate)
                fprintf(nf,"%d.0 ",itemf);
        }
        fprintf(nf,"0 ");
        fclose(f);
    }
    fclose(ft);
    fclose(fv);
    for(int i=3;i<nusers;i++)
    {
        sprintf(s,"user%d",i);
        remove(s);
    }
    delete[] mas;
}
TG *TG::copy()
{
    TG *tg=new(TG);
    tg->iuser=new(userinfo[nuserlen]);
    tg->ifilm=new(filminfo[nfilmfreq]);
    memcpy(tg->iuser,iuser,sizeof(userinfo)*nfilmfreq);
    memcpy(tg->ifilm,ifilm,sizeof(filminfo)*nfilmfreq);
}
TG* TG::rev(double left,double right)
{
    TG *tg=new(TG);
    sprintf(tg->filename,"%s_rev_%lf_%lf",filename,left,right);
    FILE *f=fopen(filename,"r");
    FILE *fo=fopen(tg->filename,"w");
    //FILE *users=new(User[])
    char s[100];
    int cu=-1;
    int trueu=-1;
    User us;
    int u,i;
    double r;
    while(fgets(s,100,f)!=0)
    {
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            if(us.nw)
            {
                User *nu=us.rev(left,right);
                nu->uir(fo);
                delete(nu);
            }
            cu=u;
            trueu++;
            us.nw=0;
            us.id=trueu;
        }

        us.watchs[us.nw].rating=r;
        us.watchs[us.nw].iitem=i;
        us.nw++;
    }
    if(us.nw)
    {
        User *nu=us.rev(left,right);
        nu->uir(fo);
        delete(nu);
    }
    fclose(f);
    fclose(fo);
    tg->count(tg->filename);
    return tg;
}
TG* TG::perm(double left,double right)
{
    TG *tg=new(TG);
    sprintf(tg->filename,"%s_perm_%lf_%lf",filename,left,right);
    FILE *f=fopen(filename,"r");
    FILE *fo=fopen(tg->filename,"w");
    char s[100];
    int cu=-1;
    int trueu=-1;
    User us;
    int u,i;
    double r;
    while(fgets(s,100,f)!=0)
    {
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
            if(us.nw)
            {
                User *nu=us.perm(left,right);
                nu->uir(fo);
                delete(nu);
            }
            us.nw=0;
            us.id=trueu;
        }

        us.watchs[us.nw].rating=r;
        us.watchs[us.nw].iitem=i;
        us.nw++;
    }
    if(us.nw)
    {
        User *nu=us.perm(left,right);
        nu->uir(fo);
        delete(nu);
    }
    fclose(f);
    fclose(fo);
    tg->count(tg->filename);
    return tg;
}
TG* TG::idf(double left,double right)
{
    TG *tg=new(TG);
    sprintf(tg->filename,"%s_id_%lf_%lf",filename,left,right);
    FILE *f=fopen(filename,"r");
    FILE *fo=fopen(tg->filename,"w");
    char s[100];
    int cu=-1;
    int trueu=-1;
    User us;
    int u,i;
    double r;
    while(fgets(s,100,f)!=0)
    {
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            cu=u;
            trueu++;
            if(us.nw)
            {
                User *nu=us.idf(left,right);
                nu->uir(fo);
                delete(nu);
            }
            us.nw=0;
            us.id=trueu;
        }

        us.watchs[us.nw].rating=r;
        us.watchs[us.nw].iitem=i;
        us.nw++;
    }
    if(us.nw)
    {
        User *nu=us.idf(left,right);
        nu->uir(fo);
        delete(nu);
    }
    fclose(f);
    fclose(fo);
    tg->count(tg->filename);
    return tg;
}
TG* TG::filmfreq(double left,double right,double nfreq,double wl=0,double l1=0)
{
    TG *tg=new(TG);
    sprintf(tg->filename,"%s_ff_%.3lf_%.3lf_%.1lf",filename,left,right,nfreq);
    FILE *f=fopen(filename,"r");
    FILE *fo=fopen(tg->filename,"w");
    char s[100];
    int cu=-1;
    int trueu=-1;
    User us;
    int u,i;
    double r;
    while(fgets(s,100,f)!=0)
    {
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            if(us.nw)
            {
                User *nu=us.ff(left,right,ifilm,nfreq);
                //User *nu=us.aug_freq(left,right,ifilm,nfreq,0,0.99);
                nu->uir(fo);
                delete(nu);
            }
            cu=u;
            trueu++;
            us.nw=0;
            us.id=trueu;
        }
        us.watchs[us.nw].rating=r;
        us.watchs[us.nw].iitem=i;
        us.nw++;
    }
    if(us.nw)
    {
        User *nu=us.ff(left,right,ifilm,nfreq);
        //User *nu=us.aug_freq(left,right,ifilm,nfreq,0,0.99);
        nu->uir(fo);
        delete(nu);
    }
    fclose(f);
    fclose(fo);
    tg->count(tg->filename);
    return tg;
}
TG* TG::filmfreq_aug(double left,double right,double nfreq,double wl=0,double l1=0.99)
{
    TG *tg=new(TG);
    sprintf(tg->filename,"%s_ff_%.3lf_%.3lf_%.1lf",filename,left,right,nfreq);
    FILE *f=fopen(filename,"r");
    FILE *fo=fopen(tg->filename,"w");
    char s[100];
    int cu=-1;
    int trueu=-1;
    User us;
    int u,i;
    double r;
    while(fgets(s,100,f)!=0)
    {
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            if(us.nw)
            {
                //User *nu=us.ff(left,right,ifilm,nfreq);
                User *nu=us.aug_freq(left,right,ifilm,nfreq,wl,l1);
                nu->uir(fo);
                delete(nu);
            }
            cu=u;
            trueu++;
            us.nw=0;
            us.id=trueu;
        }
        us.watchs[us.nw].rating=r;
        us.watchs[us.nw].iitem=i;
        us.nw++;
    }
    if(us.nw)
    {
        //User *nu=us.ff(left,right,ifilm,nfreq);
        User *nu=us.aug_freq(left,right,ifilm,nfreq,wl,l1);
        nu->uir(fo);
        delete(nu);
    }
    fclose(f);
    fclose(fo);
    tg->count(tg->filename);
    return tg;
}
TG* TG::userfreq(double left,double right,double nfreq,double wl=0,double l1=0)
{
    TG *tg=new(TG);
    sprintf(tg->filename,"%s_ff_%lf_%lf",filename,left,right);
    FILE *f=fopen(filename,"r");
    FILE *fo=fopen(tg->filename,"w");
    char s[100];
    int cu=-1;
    int trueu=-1;
    User us;
    int u,i;
    double r;
    while(fgets(s,100,f)!=0)
    {
        sscanf(s,"%d\t%d\t%lf",&u,&i,&r);
        if(cu!=u)
        {
            if((u-cu)>1)
            {
                if(us.nw)
                {
                    User *nu=us.ff(left,right,ifilm,nfreq);
                    nu->uir(fo);
                    delete(nu);
                }
            }
            cu=u;
            trueu++;
            if(us.nw)
            {
                User *nu=us.ff(left,right,ifilm,nfreq);
                nu->uir(fo);
                delete(nu);
            }
            us.nw=0;
            us.id=trueu;
        }

        us.watchs[us.nw].rating=r;
        us.watchs[us.nw].iitem=i;
        us.nw++;
    }
    fclose(f);
    fclose(fo);
    tg->count(tg->filename);
    return tg;
}
void rer(char *a,char *b)
{
    FILE *fi=fopen(a,"r");
    FILE *fo=fopen(b,"w");
    char s[100];
    while(fgets(s,100,fi)!=0)
    {
        for(int a=0;a<strlen(s);a++)
            if(s[a]==',')
                s[a]='\t';
        fprintf(fo,"%s",s);
    }
    fclose(fi);
    fclose(fo);
}
#include <QCoreApplication>
#include <stdio.h>
#include <QFile>
#include <QTextStream>
#include <QString>
#include <QStringList>
#include <QMap>
#include <QSet>
#include <locale>
#include <QTextCodec>
#include <QVector>
#include <QDateTime>
#include <QDir>
#include <QCommandLineParser>
#include <math.h>
int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    float syn=0;
    FILE *f=fopen("vectors.bin2","r");
    FILE *fo=fopen("vectors.bin3","wb");
    char s[100000];
    fgets(s,100000,f);
    int word,len;
    sscanf(s,"%d %d\n",&word,&len);
    fprintf(fo,"%lld %lld\n",word,len);
    for(int i=0;i<word;i++)
    {
        fgets(s,100000,f);
        for(int a=0;a<strlen(s);a++)
            if(s[a]=='.')s[a]=',';
        char *ss=&(s[0]);
        while (1)
        {
           fwrite(ss, 1, 1, fo);
           if(ss[0]==' ')break;
           ss=&(ss[1]);
        }
        for(int a=0;a<len;a++)
        {
            ss=&(ss[1]);
            sscanf(ss,"%f ",&syn);
            fwrite(&syn, sizeof(float), 1, fo);
            while (1)
            {
               if(ss[0]==' ')break;
               ss=&(ss[1]);
            }
        }
        fprintf(fo, "\n");
    }
    fclose(f);
    fclose(fo);




    char ss[100000];
    //FILE *ff=fopen("wiki.xml","r");
    //FILE *fo=fopen("wiki.txt","w");
    //QFile inputFile("i.xml");
    QString gline("ёйцукенгшщзхъэждлорпавыфячсмитьбюЁЙЦУКЕНГШЩЩЗХЪЭЖДЛОРПАВЫФЯЧСМИТЬБЮ1234567890*'");
    QString gline2("ёйцукенгшщзхъэждлорпавыфячсмитьбюЁЙЦУКЕНГШЩЩЗХЪЭЖДЛОРПАВЫФЯЧСМИТЬБЮ");
//    if (inputFile.open(QIODevice::ReadOnly))
//    {
//       QTextStream in(&inputFile);
//       in.setCodec("UTF-8");
//       gline = in.readLine();
//       inputFile.close();
//    }
    QFile inputFile2("wiki2.txt");
    QFile file("test3.txt");
    file.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&file);   // we will serialize the data into the file
    out.setCodec("UTF-8");

    if (inputFile2.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile2);
       in.setCodec("UTF-8");

       while (!in.atEnd())
       {
          QString line = in.readLine();
          if(gline.contains(line[0]))
          {
              QString ss("");
              int q=0;
              int l=0;
              for(int i=0;i<line.length();i++)
              {
                 if(gline2.contains(line[i]))
                 {
                         ss+=line[i];
                         q++;
                         l=1;
                 }
                 else
                     if(l)
                     {
                         ss+=' ';
                         q++;
                         l=0;
                     }
              }
              ss+='\n';
              out << ss;
          }
       }
       file.close();
       inputFile2.close();
    }

//    while(fgets(ss,100000,ff)!=0)
//    {


//        int a=0,b;
//        a=a;

//    }
   /* int *m1=new(int[1000000]);
    int *m2=new(int[1000000]);
    char s[1000];
    for(int i=0;i<100;i++)
    {
        m1[i]=0;
        m2[i]=0;
    }
    int qqq1=0;
    int qqq2=0;
    FILE *f=fopen("mlog","r");
    while(fgets(s,100,f)!=0)
    {
        int a,b;
        sscanf(s,"%d\t%d",&a,&b);
        m1[a]++;
        qqq1++;
    }
    fclose(f);
    f=fopen("itemsLstmPath","r");
    while(fgets(s,1000,f)!=0)
    {
        int a,b;
        char ss[1000];
        strcpy(ss,s);
        sscanf(s,"%d|%d",&a,&b);
        m2[a]++;
        qqq2++;
    }
    fclose(f);
    int gg=0;
    int t=0;
    for(int i=0;i<1000000;i++)
    {
        t+=m1[i];
        gg=gg+m1[i]*m2[i];
        printf("%d\n",i);
    }*/
    /*FILE *ff=fopen("lstmev_2010_ff_-1,500_-0,500_500,0_train","r");
    
    int q=0;
    while(fgets(s,900,ff)!=0)
    {
        for(int i=0;i<900;i++)
            if((s[i]==' ')&&(s[i+1]=='0'))
                q++;
    }*/

    TG tg;
//    FILE *f=fopen("mlog","r");
//    int max=0;
//    char s[1000];
//    while(fgets(s,1000,f)!=0)
//    {
//        int a,b;
//        sscanf(s,"%d %d",&a,&b);
//        if(b>max)
//            max=b;
//    }
    //rer("eventsLstmPath2q","eventsLstmPath3q");
    //tg.count("lstmev_2010");
    //TG *tg2=tg.filmfreq_aug(-1.5,-0.5,200,0,0.99);
    //tg2->nyam(tg2->perm(-1.5,-0.5))->nyam(tg2->rev(-1.5,-0.5))->fastgenerate(-1.5,-0.5);
 //   tg.count("lstmev_2010_few_us");
    //tg.abgenerate(-1.5,-0.5);
    //tg.filmfreq_aug(-1.5,-0.5,2000,1500,0.999)->fastgenerate(-1.5,-0.5);
    //tg.filmfreq_aug(-1.5,-0.5,500,0,1)->fastgenerate(-1.5,-0.5);
    //tg.filmfreq(0.5,10.5,100)->fastgenerate(0.5,10.5);
 //   tg.filmfreq_aug(0.5,10.5,500,400,0.999)->fastgenerate_a(0.5,10.5,7);
    //tg.filmfreq_aug(-1.5,-0.5,500)->fastgenerate();

    //TG *tg2=tg.rev(-1.5,-0.5);
    //tg.filmfreq(-1.5,-0.5,300)->fastgeneratex();
    //tg.fastgeneratepart(10000000);
    //tg.rev("eventsLstmPath3q");
    return 0;
}

