__author__ = 'denis'

def mean(lst):
    return sum(lst)/len(lst)
def f1(el):
    return el[1]
import numpy as np
from math import *
import pandas as pd
import matplotlib.pyplot as plt


from numpy import sin, linspace, pi
from pylab import plot, show, title, xlabel, ylabel, subplot
from scipy import fft, arange

def plotSpectrum(y,Fs,nn):
     """
     Plots a Single-Sided Amplitude Spectrum of y(t)
     """
     print(type(y))
     print(type(Fs))
     n = len(y) # length of the signal
     k = arange(n)
     T = n/Fs
     frq = k/T # two sides frequency range
     frq = frq[range(n/2)] # one side frequency range
     print('start')
     Y = fft(y)/n # fft computing and normalization
     print('end')
     Y = Y[range(n/2)]
     for i in range(100):
         print(frq[i],abs(Y)[i])
     #for i in range(100):
     #    print(frq)
     plot(frq,abs(Y),'r') # plotting the spectrum
     show()
     xlabel(str(nn))
     ylabel('|Y(freq)|')
files=['2015.09.04_16-00-20---0.484.txt','2015.09.04_16-00-35---0.506.txt','2015.09.04_16-00-50---0.527.txt','2015.09.04_16-01-06---0.500.txt','2015.09.04_16-01-21---0.516.txt','2015.09.04_16-02-52---0.506.txt','2015.09.04_16-03-08---0.506.txt','2015.09.04_16-03-23---0.516.txt','2015.09.04_16-03-39---0.527.txt','2015.09.04_16-03-57---0.533.txt','2015.09.04_16-04-13---0.516.txt','2015.09.04_16-04-29---0.511.txt','2015.09.04_16-04-43---0.538.txt','2015.09.04_16-05-02---0.473.txt','2015.09.04_16-05-30---0.506.txt','2015.09.04_16-05-47---0.522.txt','2015.09.04_16-06-19---0.506.txt','2015.09.04_16-06-35---0.511.txt','2015.09.04_16-06-51---0.495.txt','2015.09.04_16-07-07---0.473.txt','2015.09.04_16-07-23---0.506.txt','2015.09.04_16-07-38---0.522.txt']
def dis(a,b):
    return((a-b)**2)
def minimum(a,b,c):
    return min(min(a,b),c)
def dtw(s, t):
    n=len(s)
    m=len(t)
    DTW =np.array ([[0.0]*m]*n)

    for i in (range(n)[1:]):
        DTW[i, 0]=100000

    for i in (range(m)[1:]):
        DTW[0, i]=100000
    DTW[0, 0] = 0

    for i in (range(n)[1:]):
        for j in (range(m)[1:]):
            cost= dis(s[i], t[j])
            DTW[i, j] = cost + minimum(DTW[i-1, j  ],  DTW[i, j-1],  DTW[i-1, j-1])

    return DTW[n-1, m-1]

# z=dtw([1,2],[3,4])
# z=dtw([1,2],[1,2])
# z=dtw([1,2],[1,4])
# z=dtw([1,2],[2,1])
# z=dtw([1,2],[3,3])
# z=dtw([1,2,3],[1,2,3])
# z=dtw([1,2],[1,3,4])
# z=dtw([1,2],[1,1,2])
# z=dtw([1,2],[1,3,2])
X=[]
Y=[]
Z=[]
for fi in files:
    df=pd.read_csv(fi,sep='\t')
    ss=fi.split('---')
    b=ss[1].split('.txt')

    print(float(b[0]))
    Y.append(float(b[0]))
    tx=[]


    lst=df.values[:,1]
    lst3=df.values[:,0]
    #plt.plot(lst3,lst)
    #plt.show()
    lst3=lst3[lst>275]
    lst=lst[lst>275]
    lst=lst[20000:150000]
    lst3=lst3[20000:150000]



    mmin=0
    mmax=120000
    while(lst[mmin]>280):
        mmin+=1
    while(lst[mmax]>280):
        mmax-=1
    lst=lst[mmin:mmax]
    lst3=lst3[mmin:mmax]

    blst=lst[:]
    blst3=lst3[:]
    q=0
    lst=[]
    lst3=[]
    megalst=[]
    megalst3=[]
    megat=0.0
    plt.xlabel(b[0])
    for i in range(len(blst3))[1:]:
        if(blst3[i]-blst3[i-1]>0.000033):
            if(q>1000):
                n=0
                bblst=lst[:]
                bblst3=lst3[:]
                print(len(bblst3))
                print(q)
                while(((n+1)*400)<len(bblst3)):
                    lst=bblst[n*400:(n+1)*400]
                    lst2=[]
                    lst3=bblst3[n*400:(n+1)*400]
                    z=np.polyfit(lst3,lst,1)
                    print(z)
                    for i in lst3:
                        lst2.append(z[1]+i*z[0])
                    for i in range(len(lst)):
                        lst[i]=lst[i]-lst2[i]
                    #plt.plot(lst3,lst)
                    #plt.plot(lst3,lst2)
                    for i in lst:
                        megalst.append(i)
                        megalst3.append(megat)
                        megat+=0.00003
                    n+=1
            lst=[]
            lst3=[]
            q=0
            print('aaaaa'),
            print(blst3[i]-blst3[i-1],(blst3[i]-blst3[i-1])/0.000031,i)
        else:
            lst.append(blst[i])
            lst3.append(blst3[i])
            q+=1


    plt.plot(megalst3,megalst)
    plt.show()



    lst=megalst
    lst3=megalst3
    # y=[]
    # yt=[]
    # y.append(lst[0])
    # cur=lst3[0]
    # curn=1
    # yt.append(cur)
    # end=lst3[len(lst3)-1]
    # cur+=0.00003
    # while(cur<end):
    #     print(curn)
    #     while(cur>lst3[curn]):
    #         curn+=1
    #     yt.append(cur)
    #     y.append(lst[curn]*(cur-lst3[curn-1])/(lst3[curn]-lst3[curn-1])+lst[curn-1]*(lst3[curn]-cur)/(lst3[curn]-lst3[curn-1]))
    #     cur+=0.00003
    # for i in range(100):
    #     print(yt[i+1]-yt[i])
    #     print(y[i+1]-y[i])
    # # plt.plot(yt,y)
    # for i in range(len(lst)):
    #      lst[i]+=10
    # plt.plot(lst3,lst)
    # plt.plot(yt,y)
    # plt.show()
    # print(len(y))

    import scipy.signal as signal
    plotSpectrum(np.array(megalst),1.0/0.00003,float(b[0]))
    # y=[]
    # yt=[]
    # y.append(lst[0])
    # cur=lst3[0]
    # curn=1
    # yt.append(cur)
    # end=lst3[len(lst3)-1]
    # cur+=0.00003
    # while(cur<end):
    #     print(curn)
    #     while(cur>lst3[curn]):
    #         curn+=1
    #     yt.append(cur)
    #     y.append(lst[curn]*(cur-lst3[curn-1])/(lst3[curn]-lst3[curn-1])+lst[curn-1]*(lst3[curn]-cur)/(lst3[curn]-lst3[curn-1]))
    #     cur+=0.00003
    # for i in range(100):
    #     print(yt[i+1]-yt[i])
    #     print(y[i+1]-y[i])
    # # plt.plot(yt,y)
    # # for i in range(len(lst)):
    # #     lst[i]+=10
    # # plt.plot(lst3,lst)
    # # plt.show()
    # print(len(y))
    #
    # import scipy.signal as signal
    # #
    # plotSpectrum(np.array(y),1.0/0.00003,float(b[0]))
    # show()
    # plt.plot(lst3,lst)
    # plt.show()
    #
    #
    # summo=0.0
    # disp1=0.0
    # disp2=0.0
    # for i in lst:
    #     summo+=i
    # summo=summo/len(lst)
    # #print(summo)
    # for i in lst:
    #     disp1+=(i-summo)**2
    # disp1=disp1/len(lst)
    # #print(sqrt(disp1))
    # #print(len(lst),len(lst3))
    # i1=0
    # i2=0
    # while(lst[i1]>275.1):
    #     i1+=1
    # i2=i1+1000
    # while(lst[i2]>275.1):
    #     i2+=1
    # T=lst3[i2]-lst3[i1]
    # ml=max(lst)
    # #print('3 val')
    # #print(lst3[i2]-lst3[i1]),
    # # print(lst3[i2]),
    # # print(lst3[i1]),
    # #print(max(lst)),
    # #print((max(lst)-275)/(lst3[i2]-lst3[i1]))
    # #lst=lst[55000:110000]
    # #lst2=df.values[:,0]
    # #lst2=lst[(abs(lst-300)<1)]
    # #for i in lst2:
    # #    print(i)
    #
    # #print(mean(lst))
    # lst5=lst
    # lst6=lst3
    # print(len(lst))
    #
    # for j in range(2):
    #     lst2=[]
    #     lst4=[]
    #     n=2
    #     for i in range(len(lst)-n+1):
    #         lst2.append(sum(lst[a+i] for a in range(n))/n)
    #         lst4.append(lst3[i+n/2])
    #     lst=lst2
    #     lst3=lst4
    # ml2=max(lst)
    # mn2=min(lst)
    # #plt.plot(lst3,lst)
    # #plt.show()
    # #plt.plot(lst3[::100],lst[::100])
    # #plt.show()
    # # for i in range(len(lst2)-n+1):
    # #     lst5.append(sum(lst2[a+i] for a in range(n))/n)
    # #     lst6.append(sum(lst4[a+i] for a in range(n))/n)
    #
    # lst7=[]
    # for i in range(len(lst5[200:-200])):
    #     lst7.append(lst5[i+200]-lst[i+10])
    # #print(len(lst3),len(lst5),len(lst7))
    # for i in lst7:
    #     disp2+=i**2
    # disp2=disp2/len(lst7)
    # #print(sqrt(disp2))
    # #print(max(lst)),
    # #print((max(lst)-275)/(lst3[i2]-lst3[i1]))
    # tx.append(sqrt(disp2))
    # tx.append(sqrt(disp1))
    # tx.append(T)
    # tx.append(summo)
    # tx.append(ml)
    # tx.append(ml2)
    # tx.append(mn2)
    # X.append(tx)
    # Z.append(lst)

lst3=[]
lst=[]
greatlst=[]
for i in range(len(Z)):
    sl=[]
    for a in range(len(Z)):
        dist = dtw((Z[i])[::100], Z[a][::100])
        sl.append([Y[a],dist])
    greatlst.append(sl[:])
    sl.sort(key=f1)
    lst.append(sl[3][0])
    lst.append(sl[1][0])
    lst.append(sl[2][0])
    lst3.append(Y[i])
    lst3.append(Y[i])
    lst3.append(Y[i])
    print(i)

import random
for i in range(len(lst)):
    lst[i]+=random.random()*0.001
    lst3[i]+=random.random()*0.001
plt.plot(lst3,lst,"ro")
plt.show()
li=[]
for i in range(len(Y)):
    li.append([i,Y[i]])
li.sort(key=f1)
f_someset = open("matrix",'w')
f_someset.write("0")
for i in range(len(Y)):
    f_someset.write('\t'+str(li[i][1]))
f_someset.write("\n")
print(len(Y))
print(len(li))
print(len(li[0]))
for i in range(len(Y)):
    f_someset.write(str(li[i][1]))
    for a in range(len(Y)):
        print(i,a)
        f_someset.write('\t'+str(greatlst[li[i][0]][li[a][0]][1]))
    f_someset.write("\n")
f_someset.close()




import xgboost as xgb
for i in range(10):

    bb=np.random.permutation(range(len(Y)))
    data = xgb.DMatrix([X[j] for j in bb[:18]], label = [Y[j] for j in bb[:18]])
    data2 = xgb.DMatrix([X[j] for j in bb[18:]], label = [Y[j] for j in bb[18:]])
    Y1=[Y[j] for j in bb[:18]]
    Y2=[Y[j] for j in bb[18:]]
    params = { 'silent':True, 'booster':'gbtree', 'max_depth':10}
    bst = xgb.train(params, data, 10)
    bst.save_model("model.xgboost")
    fs=bst.get_fscore()
    for i in fs:
        print(i),
        print(fs[i])
    preds = bst.predict(data2)
    print(preds)
    preds = bst.predict(data)
    print(preds)
    preds=preds


    params = { 'silent':True, 'booster':'gbtree', 'max_depth':10,'alpha':0.1}
    bst = xgb.train(params, data, 10)
    bst.save_model("model.xgboost")
    fs=bst.get_fscore()
    for i in fs:
        print(i),
        print(fs[i])
    preds = bst.predict(data2)
    print(preds,Y2)
    preds = bst.predict(data)
    print(preds,Y1)
    preds=preds



    params = { 'silent':True, 'booster':'gbtree', 'max_depth':10,'lambda' : 5,'alpha':0.1}
    bst = xgb.train(params, data, 10)
    bst.save_model("model.xgboost")
    fs=bst.get_fscore()
    for i in fs:
        print(i),
        print(fs[i])
    preds = bst.predict(data2)
    print(preds,Y2)
    preds = bst.predict(data)
    print(preds,Y1)
    preds=preds



    params = { 'silent':True, 'booster':'gbtree', 'max_depth':10,'lambda' : 5}
    bst = xgb.train(params, data, 10)
    bst.save_model("model.xgboost")
    fs=bst.get_fscore()
    for i in fs:
        print(i),
        print(fs[i])
    preds = bst.predict(data2)
    print(preds,Y2)
    preds = bst.predict(data)
    print(preds,Y1)
    preds=preds
    #plt.plot(lst3[10:-10],lst7)
    #plt.show()
    # mx=max(lst)
    # mn=min(lst)
    # lst2=[]
    # lst4=[]
    # lst5=[]
    # for i in range(len(lst)):
    #     print(i)
    #     x=2*pi*(lst3[i]-lst3[i1])/(lst3[i2]-lst3[i1])
    #     print(cos(x))
    #     print(cos(x)*(mx-mn)/2)
    #     print(lst[i])
    #     lst2.append((lst[i]+cos(x)*(mx-mn)/2))
    #     lst4.append((cos(x)*(mx-mn)/2)+(mx+mn)/2)
    #     lst5.append((lst2[i]+lst[i])/2)
    # # plt.plot(lst3,lst)
    # # plt.plot(lst3,lst2)
    # plt.plot(lst3,lst5)
    # plt.show()
# df=pd.read_csv('2015.09.04_16-00-35---0.506.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# lst=lst[20000:150000]
# lst3=lst3[20000:150000]
# mmin=0
# mmax=120000
# while(lst[mmin]>280):
#     mmin+=1
# while(lst[mmax]>280):
#     mmax-=1
# lst=lst[mmin:mmax]
# lst3=lst3[mmin:mmax]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()
# df=pd.read_csv('2015.09.04_16-00-50---0.527.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# lst=lst[20000:150000]
# lst3=lst3[20000:150000]
# mmin=0
# mmax=120000
# while(lst[mmin]>280):
#     mmin+=1
# while(lst[mmax]>280):
#     mmax-=1
# lst=lst[mmin:mmax]
# lst3=lst3[mmin:mmax]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()
# df=pd.read_csv('2015.09.04_16-01-06---0.500.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# lst=lst[20000:150000]
# lst3=lst3[20000:150000]
# mmin=0
# mmax=120000
# while(lst[mmin]>280):
#     mmin+=1
# while(lst[mmax]>280):
#     mmax-=1
# lst=lst[mmin:mmax]
# lst3=lst3[mmin:mmax]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()
# df=pd.read_csv('2015.09.04_16-01-21---0.516.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# lst=lst[20000:150000]
# lst3=lst3[20000:150000]
# mmin=0
# mmax=120000
# while(lst[mmin]>280):
#     mmin+=1
# while(lst[mmax]>280):
#     mmax-=1
# lst=lst[mmin:mmax]
# lst3=lst3[mmin:mmax]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()
# df=pd.read_csv('2015.09.04_16-02-52---0.506.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# lst=lst[20000:150000]
# lst3=lst3[20000:150000]
# mmin=0
# mmax=120000
# while(lst[mmin]>280):
#     mmin+=1
# while(lst[mmax]>280):
#     mmax-=1
# lst=lst[mmin:mmax]
# lst3=lst3[mmin:mmax]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()
# df=pd.read_csv('2015.09.04_16-03-08---0.506.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()
# df=pd.read_csv('2015.09.04_16-03-23---0.516.txt',sep='\t')
# lst=df.values[:,1]
# lst3=df.values[:,0]
# lst=lst[lst>275]
# lst3=lst3[lst>275]
# print(max(lst))
# print(min(lst))
# print(mean(lst))
# plt.plot(lst3,lst)
# plt.show()