# coding: utf-8

from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction import FeatureHasher
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import LabelEncoder, LabelBinarizer
import numpy as np


def process_column_fit(name, train,dict):
    if name == 'Phrase' or name == 'bannerTitle' or name == 'criterion' or name == 'query' or name == 'bannerTitle' or name == 'bannerText' or name == 'campaignName' or name == 'PhraseStemmed':
        if name == 'PhraseStemmed':
            trains = train.applymap(lambda p: unicode(p) if type(p) != 'unicode' else p)
        else:
            trains = train.applymap(str)
        train_values = trains[name].values
        tf_vec = TfidfVectorizer(min_df=1)
        tf_train = tf_vec.fit_transform(train_values)
        lst=[]
        for i in tf_vec.get_feature_names():
            lst.append(i)
        dict[name]=lst
        return (tf_train, tf_vec,dict)

    if name == 'Date' or name == 'PhraseCount' or name == 'queryCount' or name == 'clickID':
        dict[name]=['-0']
        return (train, None,dict)
    lbl = LabelBinarizer()


    train_values = lbl.fit_transform(train[name])

    dict[name]=lbl.classes_
    return (train_values, lbl,dict)

def process_column_predict(name, train, estimator):
    if name == 'Phrase' or name == 'bannerTitle'  or name == 'criterion' or name == 'query' or name == 'bannerTitle' or name == 'bannerText' or name == 'campaignName' or name == 'PhraseStemmed':
        if name == 'PhraseStemmed':
            trains = train.applymap(lambda p: unicode(p) if type(p) != 'unicode' else p)
        else:
            trains = train.applymap(str)
        train_values = trains[name].values
        tf_train = estimator.transform(train_values)
        return tf_train

    if name == 'Date' or name == 'PhraseCount' or name == 'queryCount' or name == 'clickID':
        return train

    train_values = estimator.transform(train[name])
    return train_values
