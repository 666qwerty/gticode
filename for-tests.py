
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import random

lst = ['aaa','bb', 'cc', 'ddd', 'ee', 'fff', 'ggg', 'hh', 'kk', 'LL']
def rand_swaps(lst, p_swap):
    # some code here
    n = len(lst)
    n_lst = lst[:]
    for i in range(len(n_lst)-1):
        if (random.random() > 1-p_swap):
            tmp = n_lst[i]
            n_lst[i] = n_lst[i+1]
            n_lst[i+1] = tmp

    return n_lst

def rand_removes(lst, p_keep):
    res_lst = [x for x in lst if random.random() > (1-p_keep)]
    if (len(lst)==0):
        res_lst = lst[:]

    return res_lst

#ss = random.choice(lst,  3)
#print(ss)

#a = [1, 2, 3, 4, 5,6,7,8,9,10]
#b = [x for x in a if random.random() > 0.5]
#b[0]=111111
from numpy import sin, linspace, pi
from pylab import plot, show, title, xlabel, ylabel, subplot
from scipy import fft, arange
import random
def plotSpectrum(y,Fs):
 """
 Plots a Single-Sided Amplitude Spectrum of y(t)
 """
 print(type(y))
 print(type(Fs))
 n = len(y) # length of the signal
 k = arange(n)
 T = n/Fs
 frq = k/T # two sides frequency range
 frq = frq[range(n/2)] # one side frequency range

 Y = fft(y)/n # fft computing and normalization
 Y = Y[range(n/2)]

 plot(frq,abs(Y),'r') # plotting the spectrum
 xlabel('Freq (Hz)')
 ylabel('|Y(freq)|')

Fs = 150.0;  # sampling rate
Ts = 1.0/Fs; # sampling interval
t = arange(0,1,Ts) # time vector

ff = 50;   # frequency of the signal
y = sin(2*pi*ff*t)
for i in range(len(y)):
    if(i<len(y)/2):
        y[i]=sin(2*pi*ff*t[i])
        print('q')
    else:
        y[i]=sin(2*pi*ff*t[i]+pi)
        print('w')
subplot(2,1,1)
plot(t,y)
xlabel('Time')
ylabel('Amplitude')
subplot(2,1,2)
plotSpectrum(y,Fs)
show()


import scipy.signal as signal
f, Pper_spec = signal.periodogram(y, Fs, 'flattop', scaling='spectrum')
plt.semilogy(f, Pper_spec)
plt.xlabel('frequency [Hz]')
plt.ylabel('PSD')
plt.grid()
plt.show()


f, Pwelch_spec = signal.welch(y, Fs, scaling='spectrum')


plt.semilogy(f, Pwelch_spec)
plt.xlabel('frequency [Hz]')
plt.ylabel('PSD')
plt.grid()
plt.show()



#print(a)
#print(b)
l=[1,2,3]
print(l+[(i+3) for i in l])
#rr = rand_removes(lst, 0.5)
rr = rand_swaps(lst, 0.5)
#print(lst)
#print(rr)